﻿using System.Runtime.Serialization;

namespace DataDefinition
{
    [DataContract]
    public class PersonType
    {
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
