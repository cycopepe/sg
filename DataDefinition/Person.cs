﻿using System.Runtime.Serialization;

namespace DataDefinition
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public int PersonId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public int Type { get; set; }

    }
}
