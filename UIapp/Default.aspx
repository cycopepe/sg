﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UIapp._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%-- kendo.common.min.css contains common CSS rules used by all Kendo themes --%>
    <link href="http://cdn.kendostatic.com/2012.3.1114/styles/kendo.common.min.css" rel="stylesheet" />

    <%-- kendo.blueopal.min.css contains the "Blue Opal" Kendo theme --%>
    <link href="http://cdn.kendostatic.com/2012.3.1114/styles/kendo.default.min.css" rel="stylesheet" />

    <script src="http://cdn.kendostatic.com/2012.3.1114/js/kendo.all.min.js"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   
    <div id="grid"></div>
    
    <div id="chart"></div>
</asp:Content>
<asp:Content ID="FooterContent" runat="server" ContentPlaceHolderID="FooterContent">
    <script id="type-template" type="text/x-kendo-template">
        #=Description# 
        #if(Type==0){#
            <input type="button" value="DETAIL" class="details" data-id="#=PersonId#" data-name="#=Name#" data-age="#=Age#" data-type="#=Type#" 
        data-description="#=Description#" onclick="details(this)">
         #}#
    </script>
    
     <script>
        $(function () {
            $("#grid").kendoGrid({
                height: 400,
                columns: [
                    { field: "PersonId", width: "150px" },
                    { field: "Name", width: "150px" },
                    { field: "Age", width: "150px" },
                    { field: "Type", width: "100px", template: kendo.template($("#type-template").html()), editor: customTypeEditor },
                    { field: "Description", width: "0px", hidden: true },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" },
                ],
                pageable: true,
                sortable: true,
                filterable: false,
                editable: {
                    mode: "popup",
                    window: {
                        close: function (e) {
                            if (e.userTriggered)
                            $("#grid").data("kendoGrid").dataSource.read();
                        }
                    }
                    
                },
                toolbar: ["create",],
                edit: function (e) {
                    
                    e.container.find(".k-edit-label:last").hide();
                    e.container.find(".k-edit-field:last").hide();

                    
                },


                dataSource: {
                    
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: false,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", 
                        total: "d.Total",
                        model: {
                            id: "PersonId",
                            fields: {
                                PersonId: { editable: false, type: "number" },
                                Name: { validation: { required: true }, type: "string" },
                                Age: { type: "number" },
                                Type: { type: "number" },
                                Description: { type: "string", editable: false }
                            }
                        }
                    },
                    sort: { field: "PersonId", dir: "asc" },

                    batch: false, 
                    transport: {
                        create: {
                            url: "http://localhost/SelfHostedServiceConsole/Create",
                            contentType: "application/json; charset=utf-8", 
                            type: "POST"
                        },
                        read: {
                            url:"http://localhost/SelfHostedServiceConsole/Read",
                            contentType: "application/json; charset=utf-8",
                            type: "POST"
                        },
                        update: {
                            url: "http://localhost/SelfHostedServiceConsole/Update",
                            contentType: "application/json; charset=utf-8", 
                            type: "POST" 
                        },
                        destroy: {
                            url: "http://localhost/SelfHostedServiceConsole/Delete",
                            contentType: "application/json;  charset=utf-8",
                            type: "POST"
                        },
                        parameterMap: function(data, operation) {
                            console.log(JSON.stringify(data))
                            return JSON.stringify(data);
                        }

                    },
                    error: function(e) {
                        console.log(e)
                    },
                    requestEnd: function (e) {
                        if (e.type != "read")
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                }
                
            });

           
            
        });

        function customTypeEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataTextField: "Description",
                    dataValueField: "Type",
                    dataSource: {
                        transport: {
                            read: "http://localhost/SelfHostedServiceConsole/GetPersonTypes"
                        }
                    }
                });
        }

        function details(e) {
            var id = e.dataset.id;
            var name = e.dataset.name;
            var age = e.dataset.age;
            var type = e.dataset.description;
            $("#chart").kendoWindow({
                content: window.location.origin + "/DetailsPage.aspx?name=" + name + "&age=" + age + "&type=" + type,
                iframe: true,
                visible: false,
                height: 600,
                width: 610,
            });
            var dialog = $("#chart").data("kendoWindow");
            dialog.center();
            dialog.open();
        }

        $(".k-grid-update").click(function() {
            $("#grid").data("kendoGrid").dataSource.read();
        })

        $(".k-i-close").click(function () {
            $("#grid").data("kendoGrid").dataSource.read();
        })

        function isEmpty(o){
            for(var i in o){
                if(o.hasOwnProperty(i)){
                    return false;
                }
            }
            return true;
        }
    </script>
</asp:Content>


