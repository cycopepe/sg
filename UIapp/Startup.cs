﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UIapp.Startup))]
namespace UIapp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
