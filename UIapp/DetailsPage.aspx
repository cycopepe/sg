﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailsPage.aspx.cs" Inherits="UIapp.DetailsPage" MasterPageFile="~/Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%-- kendo.common.min.css contains common CSS rules used by all Kendo themes --%>
    <link href="http://cdn.kendostatic.com/2012.3.1114/styles/kendo.common.min.css" rel="stylesheet" />

    <%-- kendo.blueopal.min.css contains the "Blue Opal" Kendo theme --%>
    <link href="http://cdn.kendostatic.com/2012.3.1114/styles/kendo.default.min.css" rel="stylesheet" />

    <script src="http://cdn.kendostatic.com/2012.3.1114/js/kendo.all.min.js"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
   
    <asp:Label ID="nameLbl" runat="server" />
    <asp:Label ID="ageLbl" runat="server" />
    <asp:Label ID="typeLbl" runat="server" />
    
    <div id="chart"></div>
</asp:Content>
<asp:Content ID="FooterContent" runat="server" ContentPlaceHolderID="FooterContent">
   
    
    <script>
        function createChart() {
            $("#chart").kendoChart({
                title: {
                    text: "Details"
                },
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [
                    { field: "value" }
                ],
                valueAxis: {
                    max: 11,
                    line: {
                        visible: false
                    },
                    minorGridLines: {
                        visible: true
                    }
                },
                categoryAxis: {
                    categories: [],
                    majorGridLines: {
                        visible: false
                    },
                    labels: {
                        rotation: 45
                    },
                    baseUnit: "weeks",
                },
                tooltip: {
                    visible: true,
                    template: "#= series.name #: #= value #"
                }
            });

            var chart = $("#chart").data("kendoChart");
           
            for (var i = 0; i < 10; i++) {
                var randomData = Math.floor(Math.random() * (i + 1));
                chart.dataSource.add({ value: randomData });

                var randomDate = new Date(+(new Date()) - Math.floor(Math.random() * 10000000000)) + "";
                chart.options.categoryAxis.categories[i] = randomDate.substring(0, 15)
            }
          
            chart.refresh();
        }

        $(document).ready(createChart);

       
    </script>
</asp:Content>
