﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DataDefinition;
using PersonsModule;
using WcfPersonServiceLibrary.Common;

namespace WcfPersonServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        public DataSourceResult CreatePerson(string PersonId, string Name, int Age, int Type, string Description)
        {
            if (!String.IsNullOrEmpty(Description))
                return null;

            var person = new Person
            {
                Name = Name,
                Age = Age,
                Type = Type
            };

            var newPerson = PersonsOperations.Instance.CreatePerson(person);
            return new DataSourceResult
            {
                Data = PersonsOperations.Instance.GetPersons().Where(c=>c.PersonId.Equals(newPerson.PersonId)).Select(p => new PersonGrid
                {
                    PersonId = p.PersonId,
                    Name = p.Name,
                    Age = p.Age,
                    Type = p.Type,
                    Description = GetDescription(PersonsOperations.Instance, p)
                }).AsQueryable(),
                Total = 1
            };
        }
     
        public List<Person> GetAllPersons()
        {
            return PersonsOperations.Instance.GetPersons();
        }

        public List<PersonType> GetPersonTypes()
        {
            return PersonsOperations.Instance.GetPersonTypes();
        }

        public DataSourceResult Read(int skip, int take, int page, int pageSize)
        {
            return QueryableExtensions.ToDataSourceResult(PersonsOperations.Instance.GetPersons().Select(p => new PersonGrid
            {
                PersonId = p.PersonId,
                Name = p.Name,
                Age = p.Age,
                Type = p.Type,
                Description = GetDescription(PersonsOperations.Instance, p)
            }).AsQueryable(), take, skip);
        }

        private static string GetDescription(PersonsOperations po, Person p)
        {
            if (po.GetPersonTypes().Any(pt => pt.Type.Equals(p.Type)))
                return po.GetPersonTypes().FirstOrDefault(pt => pt.Type.Equals(p.Type)).Description;
            else
                return "Type not defined";
        }

        public DataSourceResult UpdatePerson(string PersonId, string Name, int Age, int Type, string Description)
        {
            var newPerson = new Person
            {
                PersonId = Int32.Parse(PersonId),
                Age = Age,
                Type = Type,
                Name = Name
            };

            var oldPerson = PersonsOperations.Instance.UpdatePerson(newPerson);

            return returnDataSource();
        }

        private DataSourceResult returnDataSource()
        {
            return QueryableExtensions.ToDataSourceResult(PersonsOperations.Instance.GetPersons().Select(p => new PersonGrid
            {
                PersonId = p.PersonId,
                Name = p.Name,
                Age = p.Age,
                Type = p.Type,
                Description = GetDescription(PersonsOperations.Instance, p)
            }).AsQueryable(), PersonsOperations.Instance.GetPersons().Count, 0);
        }

        public void DeletePerson(string PersonId, string Name, int Age, int Type, string Description)
        {
            PersonsOperations.Instance.DeletePerson(Int32.Parse(PersonId));
        }

        #region options
        public DataSourceResult ReadOptions(int skip, int take, string sort, string filter)
        {
            return returnDataSource();
        }

        public void DeletePersonOptions()
        {
            
        }

        public DataSourceResult UpdatePersonOptions()
        {
            return returnDataSource();
        }

        public void CreatePersonOptions()
        {
        }

        #endregion

    }
}
