﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using DataDefinition;
using System.ServiceModel.Web;
using WcfPersonServiceLibrary.Common;

namespace WcfPersonServiceLibrary
{
    [ServiceContract]
    public interface IService1
    {
        [WebGet(UriTemplate = "GetPersonTypes", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<PersonType> GetPersonTypes();

        [WebGet(UriTemplate = "GetAllPersons", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<Person> GetAllPersons();

        [OperationContract]
        //[WebInvoke(UriTemplate = "Read?skip={skip}&take={take}&page={page}&pageSize={pageSize}", 
        [WebInvoke(UriTemplate = "Read",
        ResponseFormat = WebMessageFormat.Json,Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [return: MessageParameter(Name = "d")]
        DataSourceResult Read(int skip, int take, int page, int pageSize);

        [WebInvoke(UriTemplate = "Create",
        ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [return: MessageParameter(Name = "d")]
        [OperationContract]
        DataSourceResult CreatePerson(string PersonId, string Name, int Age, int Type, string Description);

        [WebInvoke(UriTemplate = "Update",
        ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]
        [return: MessageParameter(Name = "d")]
        DataSourceResult UpdatePerson(string PersonId, string Name, int Age, int Type, string Description);

        [WebInvoke(UriTemplate = "Delete",
            RequestFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]
        [return: MessageParameter(Name = "d")]
        void DeletePerson(string PersonId, string Name, int Age, int Type, string Description);

        #region options hack
        [OperationContract]
        [WebInvoke(UriTemplate = "Read?skip={skip}&take={take}&sort={sort}&filter={filter}", ResponseFormat = WebMessageFormat.Json, Method = "OPTIONS")]
        DataSourceResult ReadOptions(int skip, int take, string sort, string filter);

        [WebInvoke(UriTemplate = "Create",
            ResponseFormat = WebMessageFormat.Json, Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]
        void CreatePersonOptions();

        [WebInvoke(UriTemplate = "Delete",
            ResponseFormat = WebMessageFormat.Json, Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]
        void DeletePersonOptions();

        [WebInvoke(UriTemplate = "Update",
            ResponseFormat = WebMessageFormat.Json, Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.Wrapped)]
        [OperationContract]
        DataSourceResult UpdatePersonOptions();
        #endregion
    }


}
