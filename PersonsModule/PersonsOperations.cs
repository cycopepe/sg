﻿using DataDefinition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonsModule
{
    public class PersonsOperations : IPersonsOperations
    {
        private static List<Person> Persons;
        private static List<PersonType> PersonTypes;

        private static PersonsOperations instance;

        public static PersonsOperations Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PersonsOperations();
                }
                return instance;
            }
        }

        private PersonsOperations()
        {
            Persons = new List<Person>
            {
                new Person { PersonId = 10, Name = "Jose Guzman", Age = 30, Type = 1 },
                new Person { PersonId = 20, Name = "Luis Rodriguez", Age = 30, Type = 0 }
            };

            PersonTypes = new List<PersonType>
            {
                new PersonType {Type = 0, Description = "Teacher"},
                new PersonType {Type = 1, Description = "Student"}
            };
        }

        public Person CreatePerson(Person person)
        {
            person.PersonId = Persons.Count;
            Persons.Add(person);
            return person;
        }

        public void DeletePerson(int personId)
        {
            Persons.Remove(Persons.Find(p => p.PersonId.Equals(personId)));
        }

        public PersonChart GetInfo(int personId)
        {
            var person = Persons.FirstOrDefault(p => p.PersonId.Equals(personId));
            var type = PersonTypes.FirstOrDefault(p => p.Type.Equals(person.Type));

            return new PersonChart
            {
                Name = person.Name,
                Age = person.Age,
                TypeDescription =  type.Description
            };
        }

        public List<Person> GetPersons()
        {
            return Persons;
        }

        public List<PersonType> GetPersonTypes()
        {
            return PersonTypes;
        }

        public bool UpdatePerson(Person person)
        {
            if (!Persons.Any(p => p.PersonId.Equals(person.PersonId)))
                return false;

            InnerUpdate(person);
            return true;
        }

        private void InnerUpdate(Person person)
        {
            var oldPerson = Persons.FirstOrDefault(p => p.PersonId.Equals(person.PersonId));
            Persons.Remove(Persons.Find(p => p.PersonId.Equals(person.PersonId)));

            oldPerson.Age = person.Age;
            oldPerson.Name = person.Name;
            oldPerson.Type = person.Type;
            Persons.Add(oldPerson);
        }
    }
}
