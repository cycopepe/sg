using System.Collections.Generic;
using DataDefinition;

namespace PersonsModule
{
    public interface IPersonsOperations
    {
        Person CreatePerson(Person person);
        void DeletePerson(int personId);
        List<Person> GetPersons();
        List<PersonType> GetPersonTypes();
        bool UpdatePerson(Person person);
        PersonChart GetInfo(int personId);
    }
}