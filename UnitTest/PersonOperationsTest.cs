﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonsModule;
using DataDefinition;

namespace UnitTest
{
    [TestClass]
    public class PersonOperationsTest
    {
        [TestMethod]
        public void CanCreatePersonsOperations()
        {
            var po = PersonsOperations.Instance;
            Assert.IsNotNull(po);
        }

        [TestMethod]
        public void ThereAreTwoPersonTypesByDefault()
        {
            var po = PersonsOperations.Instance;
            Assert.AreEqual(po.GetPersonTypes().Count,2);
        }

        [TestMethod]
        public void CanAddCreatePerson()
        {
            var po = PersonsOperations.Instance;
            var intialCounter = po.GetPersons().Count;
            po.CreatePerson(new Person {PersonId = 0, Age = 0, Type = 1, Name = "Test1"});
            Assert.AreEqual(po.GetPersons().Count, 1+ intialCounter);
        }

        [TestMethod]
        public void CanUpdatePerson()
        {
            var intialCounter = PersonsOperations.Instance.GetPersons().Count;
            var np = PersonsOperations.Instance.CreatePerson(new Person { PersonId = 0, Age = 0, Type = 1, Name = "Test1" });
            np.Name = "Test2";
            PersonsOperations.Instance.UpdatePerson(np);
            Assert.AreEqual(PersonsOperations.Instance.GetPersons().Count, intialCounter+1);
            Assert.AreEqual(PersonsOperations.Instance.GetPersons().FirstOrDefault(p=>p.PersonId.Equals(np.PersonId)).Name, "Test2");
        }

        [TestMethod]
        public void CantUpdateTheWrongPersonPerson()
        {
            var intialCounter = PersonsOperations.Instance.GetPersons().Count;
            var p = PersonsOperations.Instance.CreatePerson(new Person { PersonId = 0, Age = 0, Type = 1, Name = "Test1" });
            p.Name = "Test2";
            PersonsOperations.Instance.UpdatePerson(new Person { PersonId = 0, Age = 0, Type = 1, Name = "Test1" });
            Assert.AreEqual(PersonsOperations.Instance.GetPersons().Count, intialCounter+1);
            
        }

        [TestMethod]
        public void CanDeletePerson()
        {
            var intialCounter = PersonsOperations.Instance.GetPersons().Count;
            var p = PersonsOperations.Instance.CreatePerson(new Person { PersonId = 100, Age = 0, Type = 1, Name = "Test1" });
            PersonsOperations.Instance.DeletePerson(p.PersonId);
            Assert.AreEqual(PersonsOperations.Instance.GetPersons().Count, intialCounter );
        }

        [TestMethod]
        public void CantDeleteTheWrongPerson()
        {
            var intialCounter = PersonsOperations.Instance.GetPersons().Count;
            var p = PersonsOperations.Instance.CreatePerson(new Person { PersonId = 0, Age = 0, Type = 1, Name = "Test1" });
            var pToDelete = p.PersonId + 1;
            PersonsOperations.Instance.DeletePerson(pToDelete);
            Assert.AreEqual(PersonsOperations.Instance.GetPersons().Count, intialCounter+1);
        }

        [TestMethod]
        public void CanGetChartInfo()
        {
            var p = PersonsOperations.Instance.CreatePerson(new Person { PersonId = 0, Age = 0, Type = 1, Name = "Test1" });
            var chart = PersonsOperations.Instance.GetInfo(p.PersonId);
            Assert.IsNotNull(chart);
            Assert.AreEqual(chart.Age, 0);
            Assert.AreEqual(chart.Name, "Test1");
            Assert.AreEqual(chart.TypeDescription, "Student");
        }
    }
}
